package Yakuza;

import org.w3c.dom.ls.LSOutput;

public class Humain {

   private final String nom;
   private final String boisson;
   private int argent;

   //Constructeur
   public Humain(String nom, String boisson, int argent) {
      this.nom = nom;
      this.boisson = boisson;
      this.argent = argent;
   }

   //Acceseurs


   public String getNom() {
      return this.nom;
   }

   public String getBoisson() {
      return this.boisson;
   }

   public int getArgent() {
      return this.argent;
   }

   //Methodes
   public void parler(String texte) {
      System.out.println(this.getNom()+ " : " + texte);
   }

   public void direBonjour(){
      String texte = "Bonjour ! Je m'apelle "+getNom()+" et j'aime boire du "+getBoisson() ;
      parler(texte);
   }

   public void boire(){
      parler("Ahhh, un bon verre de "+getBoisson()+"! GLOUPS");
   }

   public int ajouterArgent(int somme){
      this.argent += somme ;
      return this.argent;

   }

   public int perdreArgent(int somme){
      while(somme != 0){
         somme -= 1;
      }

      return this.argent = somme;
   }

}