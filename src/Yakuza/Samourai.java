package Yakuza;

public class Samourai extends Ronin {
   //Attribut
   private String seigneur;

   //Constructeurs
   public Samourai (String nomSeigneur,String nom,String boisson,int argent){
      super(nom,boisson,argent);
      this.seigneur = nomSeigneur;
   }
   //Methodes

   public void boire(String boisson){
     super.boire();
     super.parler("Mmmmh ce "+ boisson+ "est beacoup plus raffraichissant");
   }

   @Override
   public void direBonjour(){
      super.direBonjour();
      super.parler("Mon seigneur est "+this.seigneur);
   }


}
