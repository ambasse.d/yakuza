package Yakuza;

public class Commercant extends Humain{


   //Constructeur
   public Commercant(String nom, int argent){
      super(nom,"the",argent);
   }

   //Methodes
   public int seFaireExtorquer(){
      this.perdreArgent(this.getArgent());
      return this.getArgent();
   }

   public void recevoir(int argent){
      this.ajouterArgent(argent);
   }
}
