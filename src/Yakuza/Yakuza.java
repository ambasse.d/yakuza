package Yakuza;

public class Yakuza extends Humain{
   private String clan ;
   private int reputation;

   //Contructeurs
   public Yakuza(String clan, int reputation,String nom, String boisson, int argent) {
      super(nom,boisson,argent);
      this.clan = clan;
      this.reputation= reputation +0;
   }

   //Asseceurs

   public String getClan() {
      return clan;
   }

   public int getReputation() {
      return reputation;
   }
   public void setReputation(int reputation) {
      this.reputation = reputation;
   }

   //Methodes

   public void direBonjour(){
       super.direBonjour();
       super.parler("Mon clan  est "+ this.clan);
   }

   public void extorquer(Commercant c){
      int argentCom = c.seFaireExtorquer();
      this.gagnerArg(argentCom);
      this.reputation +=1;
      super.parler("Houhou....J'ai gagné "+this.getArgent()+" euros.");
   }

   public int gagnerArg(int somme){
      this.ajouterArgent(somme);
      return this.getArgent();
   }

   public int perdreArg(){
      int somme = this.getArgent();
      this.perdreArgent(somme);
      return this.getArgent();
   }

}
