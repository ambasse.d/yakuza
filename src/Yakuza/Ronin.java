package Yakuza;

public class Ronin extends Humain {
   //Atribut
   int honneur = 1;

   //Constructeur
   public Ronin(String nom,String boisson,int argent){
      super(nom,boisson,argent);

   }

   //Asseceurs

   public int getHonneur() {
      return honneur;
   }

   public void setHonneur(int honneur) {
      this.honneur = honneur;
   }

   //Methodes
   public int donnerArgent(Commercant c,int valeur){
      this.ajouterArgent(valeur);
      return this.getArgent();
   }

   public void provoquer(Yakuza yaku){

      if ((2*honneur)>yaku.getReputation()){
         //Ronin Gagne
         int victoire = yaku.getArgent();
         this.ajouterArgent(victoire);
         yaku.perdreArg();
         this.honneur +=1;
         this.parler("Houhou J'ai gagner "+this.getArgent()+" contre " + yaku.getNom());
      }else{
         this.honneur -=1;
         int defaite = this.getArgent();
         yaku.gagnerArg(defaite);
         this.parler("Thoow j'ai perdu");
      }
   }

}
